const express = require('express')
const router = express.Router()
const UsersController = require('../controller/UsersController')

router.get('/', UsersController.getUsers)
router.get('/', UsersController.getUsers)
router.get('/:id', UsersController.getUser)
router.post('/', UsersController.addUser)
router.put('/', UsersController.updateUser)
router.delete('/:id', UsersController.deleteUser)

module.exports = router
